//
//  LocationManager.swift
//  Open Weather
//
//  Created by Guilherme Pereira de Freitas on 2020-01-09.
//  Copyright © 2020 Guilherme Pereira de Freitas. All rights reserved.
//

import Combine
import CoreLocation
import Foundation

final class LocationManager: NSObject {
    private let manager = CLLocationManager()
    
    let isAuthorized = PassthroughSubject<Bool, Never>()
    let currentLocation = PassthroughSubject<CLLocation, Error>()
    
    override init() {
        super.init()
        
        manager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else { return }
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    func start() {
        guard CLLocationManager.locationServicesEnabled() else { return }
        manager.startUpdatingLocation()
    }
    
    func stop() {
        manager.stopUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        isAuthorized.send(status == .authorizedWhenInUse || status == .authorizedAlways)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation.send(locations.last!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        currentLocation.send(completion: .failure(error))
    }
}
