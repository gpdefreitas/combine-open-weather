//
//  ViewController.swift
//  Open Weather
//
//  Created by Guilherme Pereira de Freitas on 2020-01-08.
//  Copyright © 2020 Guilherme Pereira de Freitas. All rights reserved.
//

import Combine
import UIKit

class WeatherViewController: UIViewController {
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet weak var currentConditionIcon: UIImageView!
    @IBOutlet weak var currentTemperature: UILabel!
    @IBOutlet weak var maxTemperature: UILabel!
    @IBOutlet weak var minTemperature: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var cancellableSet = Set<AnyCancellable>()
    let viewModel = WeatherViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViewModel()
    }
    
    @IBAction func search(_ sender: UITextField) {
        if let text = sender.text {
            viewModel.location = Location(city: text)
        } else {
            viewModel.location = nil
        }
        sender.resignFirstResponder()
    }
    
    @IBAction func findMyLocation(_ sender: UIButton) {
        viewModel.startUpdatingLocation()
    }
    
    func bindViewModel() {
        viewModel.currentCity
            .receive(on: DispatchQueue.main)
            .assign(to: \.text, on: city)
            .store(in: &cancellableSet)
        
        viewModel.currentTemperature
            .receive(on: DispatchQueue.main)
            .assign(to: \.text, on: currentTemperature)
            .store(in: &cancellableSet)

        viewModel.maxTemperature
            .receive(on: DispatchQueue.main)
            .assign(to: \.text, on: maxTemperature)
            .store(in: &cancellableSet)

        viewModel.minTemperature
            .receive(on: DispatchQueue.main)
            .assign(to: \.text, on: minTemperature)
            .store(in: &cancellableSet)

        viewModel.icon
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: currentConditionIcon)
            .store(in: &cancellableSet)
        
        viewModel.isSearching
            .receive(on: DispatchQueue.main)
            .map { !$0 }
            .assign(to: \.isHidden, on: activityIndicator)
            .store(in: &cancellableSet)

        Publishers.CombineLatest(viewModel.locationButtonIcon, viewModel.isLocationAuthorized)
            .receive(on: DispatchQueue.main)
            .map { [weak self] (image, isAuthorized) in
                self?.myLocationButton.setImage(image, for: .normal)
                return isAuthorized
            }
            .assign(to: \.isEnabled, on: myLocationButton)
            .store(in: &cancellableSet)
    }
}
