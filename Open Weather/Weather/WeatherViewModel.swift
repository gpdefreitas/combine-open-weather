//
//  WeatherViewModel.swift
//  Open Weather
//
//  Created by Guilherme Pereira de Freitas on 2020-01-08.
//  Copyright © 2020 Guilherme Pereira de Freitas. All rights reserved.
//

import Combine
import CoreLocation
import Foundation
import UIKit

class WeatherViewModel {
    let currentCity = PassthroughSubject<String?, Never>()
    let currentTemperature = PassthroughSubject<String?, Never>()
    let maxTemperature = PassthroughSubject<String?, Never>()
    let minTemperature = PassthroughSubject<String?, Never>()
    let icon = PassthroughSubject<UIImage?, Never>()
    let isSearching = PassthroughSubject<Bool, Never>()
    
    let isLocationAuthorized: PassthroughSubject<Bool, Never>
    var locationButtonIcon: AnyPublisher<UIImage, Never> {
        locationManager.isAuthorized
            .map{
                let imageSystemName = $0 ? "location.fill" : "location.slash.fill"
                return UIImage(systemName: imageSystemName)!
            }
            .eraseToAnyPublisher()
    }
    
    var location: Location? {
        didSet {
            self.locationManager.stop()

            guard let location = location else { return }
            updateLocation(location: location)
        }
    }

    private let openWeather = OpenWeather()
    private let locationManager = LocationManager()

    private var cancellableSet = Set<AnyCancellable>()

    init() {
        locationManager.start()
        isLocationAuthorized = locationManager.isAuthorized
        
        locationManager.currentLocation
            .sink(
                receiveCompletion: { [weak self] completion in
                    guard case .failure = completion else { return }
                    self?.location = nil
                },
                receiveValue: { [weak self] currentLocation in
                    self?.location = Location(location: currentLocation.coordinate)
            })
            .store(in: &cancellableSet)
    }
    
    func startUpdatingLocation() {
        locationManager.start()
    }
    
    private func updateLocation(location: Location) {
        isSearching.send(true)
        openWeather.currentWeather(in: location)
            .sink(
                receiveCompletion: { [weak self] completion in
                    guard let self = self else { return }
                    self.isSearching.send(false)

                    guard case .failure = completion else { return }
                    self.currentCity.send("")
                    self.currentTemperature.send("-- ºC")
                    self.maxTemperature.send("-- ºC")
                    self.minTemperature.send("-- ºC")
                    self.icon.send(nil)
                },
                receiveValue: { [weak self] weather in
                    guard let self = self else { return }
                    self.currentCity.send(weather.city.name)
                    self.currentTemperature.send(weather.detail.temperature.formattedTemperature)
                    self.maxTemperature.send(weather.maximum.formattedTemperature)
                    self.minTemperature.send(weather.minimum.formattedTemperature)
                    self.updateIcon(weather.detail.condition.icon)
        })
        .store(in: &cancellableSet)
    }
    
    private func updateIcon(_ name: String) {
        openWeather.image(for: name)
            .replaceError(with: UIImage())
            .sink(
                receiveValue: { [weak self] image in
                    self?.icon.send(image)
            })
        .store(in: &cancellableSet)
    }
}

private extension Temperature {
    private static var temperatureFormatter: MeasurementFormatter {
        let formatter = MeasurementFormatter()
        formatter.unitStyle = .medium
        formatter.unitOptions = .providedUnit
        return formatter
    }
    private static var unitTemperature = UnitTemperature.celsius
    
    var formattedTemperature: String {
        return Temperature.temperatureFormatter.string(from: self.converted(to: Temperature.unitTemperature))
    }
}
