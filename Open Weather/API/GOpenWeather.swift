import Combine
import CoreLocation
import Foundation
import UIKit

final class OpenWeather {
    private let feedProvider: FeedProviding
    private let urlSession: URLSession
    private let decoder: JSONDecoder
        
    init(location: Location? = nil,
         feedProvider: FeedProviding = DefaultFeedProvider(),
         urlSession: URLSession = URLSession.shared,
         decoder: JSONDecoder = JSONDecoder()) {
        
        self.feedProvider = feedProvider
        self.urlSession = urlSession
        self.decoder = decoder
    }
    
    func currentWeather(in location: Location) -> AnyPublisher<Weather, Error> {
        let currentWeather = CurrentWeather(location: location)
        let url = feedProvider.feedURL(for: currentWeather)!
        return urlSession.dataTaskPublisher(for: url)
            .tryMap { result -> Weather in
                return try self.decoder.decode(Weather.self, from: result.data)
            }
            .eraseToAnyPublisher()
    }
    
    private let iconBaseURL = "https://openweathermap.org/img/wn/"
    private let iconExtension = "png"

    func image(for icon: String) -> AnyPublisher<UIImage, URLError> {
        var url = URL(string: iconBaseURL)
        url?.appendPathComponent("\(icon)@2x.\(iconExtension)", isDirectory: false)
        
        return urlSession.dataTaskPublisher(for: url!)
            .map { UIImage(data: $0.data) ?? UIImage() }
            .eraseToAnyPublisher()
    }
}
