import CoreLocation
import Foundation

public protocol FeedProviding {
    var version: Version { get }
    var baseURL: URL { get }
    var apiKey: String { get }
    
    func feedURL<T: Resource>(for resource: T) -> URL?
}

public extension FeedProviding {
    func feedURL<T: Resource>(for resource: T) -> URL? {
        var baseURL = self.baseURL
        baseURL.appendPathComponent(version.rawValue)
        baseURL.appendPathComponent(T.name)
        
        var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)
        var queryItems = resource.queryItems
        queryItems.append(URLQueryItem(name: "APPID", value: apiKey))
        components?.queryItems = queryItems

        return components?.url
    }
}
