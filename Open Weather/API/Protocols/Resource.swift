import Foundation

public protocol Resource: class {
    static var name: String { get }
    var location: Location { get }
}

public extension Resource {
    var queryItems: [URLQueryItem] {
        var queryItems = [URLQueryItem]()
        if var queryString = location.city {
            if let country = location.country {
                queryString.append(",\(country)")
            }
            queryItems.append(URLQueryItem(name: "q", value: queryString))
        } else if let coordinate2D = location.coordinate2D {
            queryItems.append(URLQueryItem(name: "lat", value: "\(coordinate2D.latitude)"))
            queryItems.append(URLQueryItem(name: "lon", value: "\(coordinate2D.longitude)"))
        }
        return queryItems
    }
}
