//
//  City.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-03.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import CoreLocation

public struct City: Codable {
    //MARK: - Public properties
    let name: String
    let province: Province? = nil
    let openWeatherID: String? = nil
    let weatherCanadaCode: String? = nil
    var location: CLLocationCoordinate2D? {
        guard let coordinate = coordinate else { return nil }
        return CLLocationCoordinate2D(latitude: coordinate.latitude,
                                      longitude: coordinate.longitude)
    }

    var alerts: [Alert]? = nil
    
    //MARK: - Private properties
    private let coordinate: Coordinate? = nil
    
    enum CodingKeys: String, CodingKey {
        case name
        case openWeatherID = "id"
        case coordinate = "coord"
    }
}

//MARK: - Helper struct
private struct Coordinate: Codable {
    let latitude: Double
    let longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
    }
}
