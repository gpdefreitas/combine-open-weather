//
//  Weather.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-03.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

import Foundation

public struct Weather: Codable {
    //MARK: Public vars
    var city: City
    let date: Date
    let detail: WeatherDetail
    
    //MARK: Computed vars
    var minimum: Temperature {
        return Temperature(value: main.temp_min, unit: UnitTemperature.kelvin)
    }
    var maximum: Temperature {
        return Temperature(value: main.temp_max, unit: UnitTemperature.kelvin)
    }
    var sunrise: Date { return sys.sunrise }
    var sunset: Date { return sys.sunset }

    //MARK: Private vars
    private let main: WeatherMain
    private let sys: WeatherSys
    
    enum CodingKeys: String, CodingKey {
        case main, sys
        case date = "dt"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = try container.decode(Date.self, forKey: .date)
        sys = try container.decode(WeatherSys.self, forKey: .sys)
        main = try container.decode(WeatherMain.self, forKey: .main)
        
        detail = try WeatherDetail(from: decoder)
        city = try City(from: decoder)

    }
}

//MARK: - Helper struct
private struct WeatherMain: Codable {
    let temp: Double
    let humidity: Double
    let temp_min: Double
    let temp_max: Double
}

private struct WeatherSys: Codable {
    let sunrise: Date
    let sunset: Date
}
