//
//  Province.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-03.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

public struct Province : Codable {
    let name: String
    let abbreviation: String
}
