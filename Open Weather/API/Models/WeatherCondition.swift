//
//  WeatherCondition.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-03.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

public struct WeatherCondition: Codable {
    let id: Double
    let main: String
    let description: String
    let icon: String
}
