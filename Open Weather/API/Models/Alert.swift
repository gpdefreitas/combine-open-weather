//
//  Alert.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-15.
//  Copyright © 2018 gpdef. All rights reserved.
//
import Foundation

public enum AlertType: String, Codable {
    case warning, watch, statement
}

public struct Alert: Codable {
    let id: String
    let title: String
    let summary: String
    let updated: Date
    let url: URL
    let isInEffect: Bool
    let type: AlertType
}
