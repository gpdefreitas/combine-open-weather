//
//  Wind.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-03.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//

public struct Wind: Codable {
    let speed: MetersPerSecond
    let direction: Degree?
    let gust: KilometersPerHour?
    
    enum CodingKeys: String, CodingKey {
        case speed, gust
        case direction = "deg"
    }
}
