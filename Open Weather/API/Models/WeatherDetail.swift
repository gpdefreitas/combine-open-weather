//
//  WeatherDetail.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-03.
//  Copyright © 2018 gpdefreitas. All rights reserved.
//
import Foundation

public struct WeatherDetail: Codable {
    //MARK: Public properties
    let visibility: Meters?
    let wind: Wind
    
    //MARK: Computed vars
    var humidity: Percentage { return main.humidity }
    var cloudiness: Percentage { return clouds.all }
    var rainLast3Hours: Millimeters? { return rain?.last3hours }
    var snowLast3Hours: Millimeters? { return snow?.last3hours }
    var temperature: Temperature { return Temperature(value: main.temp, unit: UnitTemperature.kelvin) }
    var condition: WeatherCondition { return weather.first! }

    //MARK: Private vars
    private let main: WeatherMain
    private let clouds: Clouds
    private let rain: Precipitation?
    private let snow: Precipitation?
    private let weather: [WeatherCondition]
}

private struct WeatherMain: Codable {
    let humidity: Double
    let temp: Double
}

private struct Clouds: Codable {
    let all: Double
}

private struct Precipitation: Codable {
    let last3hours: Double?
    
    enum CodingKeys: String, CodingKey {
        case last3hours = "3h"
    }
}
