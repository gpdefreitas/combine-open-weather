import Foundation

public struct DefaultFeedProvider: FeedProviding {
    public var version: Version = .v2_5
    public var baseURL: URL = URL(string: "https://api.openweathermap.org/data/")!
    
    //App's info plist should have key GOpenWeatherAPIKey
    //9be893e2dd555134f4874a4e3d058a30

    public var apiKey: String = "9be893e2dd555134f4874a4e3d058a30" //Bundle.main.object(forInfoDictionaryKey: "GOpenWeatherAPIKey") as? String ?? ""
}
