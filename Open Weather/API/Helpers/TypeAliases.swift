//
//  TypeAliases.swift
//  En Route Weather
//
//  Created by Guilherme de Freitas on 2018-09-03.
//  Copyright © 2018 gpdef. All rights reserved.
//
import Foundation

typealias MetersPerSecond = Double
typealias KilometersPerHour = Double
typealias Degree = Double
typealias Percentage = Double
typealias Meters = Double
typealias Millimeters = Double
typealias Temperature = Measurement<UnitTemperature>

/*
public extension Measurement where UnitType : UnitTemperature {
}
*/
