import CoreLocation
import Foundation

public struct Location {
    let city: String?
    let country: String?
    let coordinate2D: CLLocationCoordinate2D?
    
    init(city: String, country: String? = nil) {
        self.init(city: city, country: country, location: nil)
    }
    
    init(location: CLLocationCoordinate2D) {
        self.init(city: nil, country: nil, location: location)
    }
    
    private init(city: String?, country: String?, location: CLLocationCoordinate2D?) {
        self.city = city
        self.country = country
        self.coordinate2D = location
    }
}
