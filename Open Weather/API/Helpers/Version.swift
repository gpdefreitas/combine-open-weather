import Foundation

public enum Version: String {
    case v2_5 = "2.5"
}
