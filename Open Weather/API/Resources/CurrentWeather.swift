import Foundation

public class CurrentWeather: Resource {
    init(location: Location) {
        self.location  = location
    }
    
    public static var name: String = "weather"

    public var location: Location
}
